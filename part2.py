import part1

print("\n\npart 2")

class Stack:
    def __init__(self):
        self.stack = []
    
    def pop(self):
        item = self.stack[len(self.stack) - 1]
        self.stack.remove(item)
        return item
    
    def push(self, item):
        self.stack.append(item)
        return None

def BinaryTreeExpressionToReversePolish(node):
    if isinstance(node, str):
        return node
    elif isinstance(node, part1.Node):
        RPN = BinaryTreeExpressionToReversePolish(node.left)
        RPN += BinaryTreeExpressionToReversePolish(node.right)
        RPN += node.op
        return RPN
    return None        

def EvalOp(operator, operand1, operand2):
    operand1 = int(operand1)
    operand2 = int(operand2)
    if operator == "/":
        return operand1 / operand2
    elif operator == "*":
        return operand1 * operand2
    elif operator == "+":
        return operand1 + operand2
    elif operator == "-":
        return operand1 - operand2
    return None

def EvaluateRPN(expression):
    stack = Stack()
    for item in expression:
        if part1.is_number(item):
            stack.push(item)
        else:
            operand2 = stack.pop()
            operand1 = stack.pop()
            stack.push(EvalOp(item, operand1, operand2))
    return stack.pop()

expressionTree = part1.ExpressionTree(part1.arithmetic_input)
print(expressionTree)
expressionRPN = BinaryTreeExpressionToReversePolish(expressionTree)
print(expressionRPN)
evaluated = EvaluateRPN(expressionRPN)
print(evaluated)

