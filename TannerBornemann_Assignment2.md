# Algorithms Assignment 2

```
Tanner Bornemann
Professor Madhav Lolla
Design & Analysis of Algorithms
July 09 2018
```

Below are my solutions to both parts of Assignment 2 written in pseudo code. The bottom of this document contains working python code that is based on my pseudo code.

A fully rendered version of this document can be found here: https://gitlab.com/FalsePsyche/AlgorithmsClass/blob/master/TannerBornemann_Assignment2.md

## Part 1

The ExpressionTree function will take an arithmetic expression (e.g. (3+2*6)/(8-5)) and transform it into a representative binary tree.

```pseudocode
ExpressionTree(arithmeticExpression):
    node = (left=null, value=null, right=null)
    if first item in arithmeticExpression is a number:
        node.left = first item of arithmeticExpression
        node.value = second item of arithmeticExpression
        node.right = ExpressionTree(all items of arithmeticExpression excluding the first and second items)
    else if first of arithmeticExpression is open parenthesis:
        closeId = index position of closing parenthesis that matches the open parenthesis within arithmeticExpression
        if the length of arithmeticExpression is greater than closeId - 1:
            node.left = ExpressionTree(arithmeticExpression excluding the first item and all items after the index position of closeId)
            node.value = item at index position closeId + 1 in arithmeticExpression
            node.right = all items of arithmeticExpression from the index position of closeId + 2 and to the end of the list
        else:
            node = ExpressionTree(arithmeticExpression excluding the first item and the last item)
    return node
```

## Part 2

This function will take the Binary Expression Tree and turn it into a Reverse Polish Notation expression.

```pseudocode
GetRPM(BinaryExpressionTree): // returns a RPN equivalent of the input
    if left node of BinaryExpressionTree is empty: // recursion base case: we found a leaf node so return the value
        return value of BinaryExpressionTree
    else: // this node has children so we need to dig deeper (assuming it has two children)
        RPN = GetRPN(left node of BinaryExpressionTree) + GetRPN(right node of BinaryExpressionTree) + value of BinaryExpressionTree // compose the Reverse Polish Notation for this node, recursively
        return RPN
```

This function will evaluate the Reverse Polish Notation expression and return with a final result

```pseudocode
EvaluateRPN(expression):
    stack = new empty stack
    for each item in expression: // loop through all items in the expression
        if this item is a number: // if this item is a number then push it to stack
            stack.push(item)
        else: // if this item is not a number then get that last two numbers from the stack and then evaluate the two numbers with this operator
            operand2 = stack.pop()
            operand1 = stack.pop()
            stack.push(Compute operand1 with item (aka the operator) with operand2)
    return stack.pop() // return the result, which is the only item on the stack
```

## Python Code

```python

# Tanner Bornemann
# Professor Madhav Lolla
# Design & Analysis of Algorithms
# July 09 2018
# Assignment 2

# Part 1 Algorithm

arithmetic_input = "(3+2*6)/(8-5)"

class Node:

    def __init__(self, left=None, op=None, right=None):
        self.left = left
        self.op = op
        self.right = right

    def __str__(self):
        """Override str func for printing in console, no this is not pretty."""
        pretty = ""
        pretty += "\n[ {} ]".format(self.op)
        pretty += "\n|\t\\"
        if isinstance(self.left, Node):
            pretty += "\t{}".format(str(self.left))
        else:
            pretty += "\n[{}]".format(self.left)
        if isinstance(self.right, Node):
            pretty += str(self.right)
        else:
            pretty += "\t[{}]".format(self.right)
        pretty += ""
        return pretty

def ExpressionTree(arithmetic):
    """This algorithm treats the input as if it were a list of items rather than a raw string.
    This means that it cannot handle numbers with more than one digit or spaces.
    This algorithm also cannot handle nested parenthesis.
    And this algorithm assumes that the input is formatted correctly (i.e. follows the pattern of operand1, operator, then operand2)"""
    if arithmetic.find("((") > 0:
        return arithmetic # this algorithm cannot handle nested parentheses
    node = Node()
    if is_number(arithmetic[0]):
        if len(arithmetic) == 1:
            return arithmetic[0]
        else:
            node.left = arithmetic[0]
            node.op = arithmetic[1]
            node.right = ExpressionTree(arithmetic[2:])
    elif arithmetic[0] == "(":
        closeId = arithmetic.find(")")
        if closeId == len(arithmetic) - 1:
            node = ExpressionTree(arithmetic[1:-1])
        else:
            node.left = ExpressionTree(arithmetic[1:closeId])
            node.op = arithmetic[closeId + 1]
            node.right = ExpressionTree(arithmetic[closeId+2:])
    return node

def is_number(val): # quick and dirty filter
    if val == "(" or val == ")" or val in ["*", "/", "+", "-"]:
        return False
    if 0 <= int(val) <= 9:
        return True
    return False

print(ExpressionTree(arithmetic_input))

# Part 2 Algorithm

class Stack:

    def __init__(self):
        self.stack = []

    def pop(self):
        item = self.stack[len(self.stack) - 1]
        self.stack.remove(item)
        return item

    def push(self, item):
        self.stack.append(item)

def BinaryTreeExpressionToReversePolish(node):
    if isinstance(node, str):
        return node
    elif isinstance(node, Node):
        RPN = BinaryTreeExpressionToReversePolish(node.left)
        RPN += BinaryTreeExpressionToReversePolish(node.right)
        RPN += node.op
        return RPN
    return None

def EvaluateRPN(expression):
    stack = Stack()
    for item in expression:
        if is_number(item):
            stack.push(item)
        else:
            operand2 = stack.pop()
            operand1 = stack.pop()
            stack.push(eval(str(operand1) + str(item) + str(operand2)))
    return stack.pop()

expressionTree = ExpressionTree(arithmetic_input)
print(expressionTree)
expressionRPN = BinaryTreeExpressionToReversePolish(expressionTree)
print(expressionRPN)
evaluated = EvaluateRPN(expressionRPN)
print(evaluated)


```