print("helloworld")

arithmetic_input = "(3+2*6)/(8-5)"


class Node:

    def __init__(self, left=None, op=None, right=None):
        self.left = left
        self.op = op
        self.right = right

    def __str__(self):
        pretty = ""
        pretty += "\n[ {} ]".format(self.op)
        pretty += "\n|\t\\"
        if isinstance(self.left, Node):
                        pretty += "\t{}".format(str(self.left))
        else:
            pretty += "\n[{}]".format(self.left)
        if isinstance(self.right, Node):
            pretty += str(self.right)
        else:
            pretty += "\t[{}]".format(self.right)
        pretty += ""
        return pretty
            

    def pretty_print(self):      
        # print("\n\nArithmetic Expression Tree\n\n[{}]\n\n[{}]\n\n[{}]".format(self.left, self.op, self.right))
        print("\n\nArithmetic Expression Tree\n\n[{}]".format(self))
        

def ExpressionTree(arithmetic):
    if arithmetic.find("((") > 0:
        return arithmetic # this algorithm cannot handle nested parentheses
    node = Node()
    if is_number(arithmetic[0]):
        if len(arithmetic) == 1:
            return arithmetic[0]
        else:        
            node.left = arithmetic[0]
            node.op = arithmetic[1]
            node.right = ExpressionTree(arithmetic[2:])
    elif arithmetic[0] == "(":
        closeId = arithmetic.find(")")
        if closeId == len(arithmetic) - 1:
            node = ExpressionTree(arithmetic[1:-1])
        else:
            node.left = ExpressionTree(arithmetic[1:closeId])
            node.op = arithmetic[closeId + 1]
            node.right = ExpressionTree(arithmetic[closeId+2:])
    return node

def is_number(val):
    if val == "(" or val == ")":
        return False
    if val in ["*", "/", "+", "-"]:
        return False
    if 0 <= int(val) <= 9:
        return True
    else:
        return False


def ValidateExpression(expression):
    """
    >>> ValidateExpression("()")
    True
    >>> ValidateExpression("(")
    False
    >>> ValidateExpression(")")
    False
    >>> ValidateExpression("")
    True
    """
    openCount = 0
    closeCount = 0
    for char in expression:
        if char == "(":
            openCount += 1
        elif char == ")":
            closeCount += 1
    if openCount == closeCount:
        return True
    else:
        return False

# check if valid
# print("Expression: '{}' is valid: {}".format(
#     arithmetic, ValidateExpression(arithmetic)))
node = ExpressionTree(arithmetic_input)
node.pretty_print()